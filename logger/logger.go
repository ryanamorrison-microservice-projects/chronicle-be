package logger 

import(
  "fmt"
  "io/fs"
  "log"
  "os"
)

const logExt = "log"
const appName = "chronicle"
const errorLogFilenamePart = "error" 
const warnLogFilenamePart = "warn" 
const infoLogFilenamePart = "info"
const fileOptions = os.O_APPEND | os.O_CREATE | os.O_WRONLY
const filePerms fs.FileMode = 0666
const logOptions = log.Ldate | log.Ltime | log.LUTC | log.Lmicroseconds | log.Lmsgprefix

//basic implementations for now using Syslog
type MessageLogger struct {
  // contains filtered or unexported fields
  appName            string
  appPartName        string
  errorLogFileName   string
  warnLogFileName    string
  infoLogFileName    string
  errorPrefix        string
  warnPrefix         string
  infoPrefix         string
}

//function creates a log file (if needed) and then sets the logging format, returns a log.Logger
func LogEvent(fileName, prefix, event string) (error) {
  if len(fileName) == 0 || len(prefix) == 0 {
    return fmt.Errorf("Error: supplied filename or prefix is nil, use factory function to create logger.")
  }
  if len(event) == 0 {
    return fmt.Errorf("Error: supplied event was empty.  Nothing to do.")
  } 
  file, err := os.OpenFile(fileName, fileOptions, filePerms)
  if err != nil {
    return err
  }
  defer file.Close()
  logger := log.New(file, prefix, logOptions)
  logger.Println(event)
  return nil
}

//logs an Error message
func (ml MessageLogger) ErrorLogger(event string) (error) {
  err := LogEvent(ml.errorLogFileName, ml.errorPrefix, event)
  if err != nil {
    return err
  }
  return nil
}
//logs a Warning message
func (ml MessageLogger) WarnLogger(event string) (error) {
  err := LogEvent(ml.warnLogFileName, ml.warnPrefix, event)
  if err != nil {
    return err
  }
  return nil
}
//logs an Info message
func (ml MessageLogger) InfoLogger(event string) (error) {
  err := LogEvent(ml.infoLogFileName, ml.infoPrefix, event)
  if err != nil {
    return err
  }
  return nil
} 

//these three functions are mostly to make testing easier
//function returns the name of the Error Log file
func (ml MessageLogger) ErrorLogFileName() (string) {
  return ml.errorLogFileName
}

//function returns the name of the Warning Log file
func (ml MessageLogger) WarnLogFileName() (string) {
  return ml.warnLogFileName
}

//function returns the name of the Info Log file
func (ml MessageLogger) InfoLogFileName() (string) {
  return ml.infoLogFileName
}

//factory function to create a new logger
func NewLogger(appPartName string) (MessageLogger, error) {
  var ml = MessageLogger{
    appName: appName, 
    appPartName: appPartName,
    errorLogFileName: fmt.Sprintf("%s-%s.%s", appName, errorLogFilenamePart, logExt),
    warnLogFileName: fmt.Sprintf("%s-%s.%s", appName, warnLogFilenamePart, logExt),
    infoLogFileName: fmt.Sprintf("%s-%s.%s", appName, infoLogFilenamePart, logExt),
    errorPrefix: fmt.Sprintf("[%s] ERROR: ", appPartName),
    warnPrefix: fmt.Sprintf("[%s] WARNING: ", appPartName), 
    infoPrefix: fmt.Sprintf("[%s] INFO: ", appPartName),
  }
  return ml, nil
}
