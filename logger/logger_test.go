package logger 

import(
  "fmt"
  "os"
  "testing"
)
//============test data======
var appPartName string = "testing_app"
var removeFiles bool = true 

//============tests==========
func TestSysLogger(t *testing.T) {
  sysLogger, err := NewLogger(appPartName)
  if err != nil {
    t.Error(err)
  }

  err = sysLogger.ErrorLogger("test error logger")
  if err != nil {
    t.Error(err)
  }
  _ = sysLogger.ErrorLogger("additional test error logger")
  err = sysLogger.WarnLogger("test warn logger")
  if err != nil {
    t.Error(err)
  }
  _ = sysLogger.WarnLogger("additional test warn logger")
  err = sysLogger.InfoLogger("test info logger")
  if err != nil {
    t.Error(err)
  }
  _ = sysLogger.InfoLogger("additional test info logger")

  //missing test to see if files are being created
  if removeFiles == true {
    os.Remove(sysLogger.ErrorLogFileName())
    os.Remove(sysLogger.WarnLogFileName())
    os.Remove(sysLogger.InfoLogFileName())
  }
}

func TestEmptyLogger(t *testing.T) {
  var sysLogger MessageLogger
  err := sysLogger.ErrorLogger("test error logger")
  if err == nil {
    t.Error(fmt.Errorf("Testing Error: message logging should have returned an error but it returned nil.\n"))
  } /*else {
    fmt.Printf("Testing empty logger error succeeded: (%s)\n", err) 
  } */
}
