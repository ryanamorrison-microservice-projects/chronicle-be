package logic

//interface of Logger for ChronicleLogic
type LogicLogger interface {
  ErrorLogger(message string) (error)
  WarnLogger(message string) (error)
  InfoLogger(message string) (error)
}

