package logic

import(
  "encoding/json"
)

//other stuct json definitions in logic_create.go

type ChronicleList struct {
  Chronicles []Chronicle `json:"chronicles"`
}

type ChronicleName struct {
  Name string `json:"name"`
}

type Chronicles struct {
  NumberOfChronicles int `json:"number_of_chronicles"`
}

func (cl ChronicleLogic) NumberOfChronicles() ([]byte) {
  nc := Chronicles{
    NumberOfChronicles: cl.crs.Len(),
  }
  out, jsonErr := json.Marshal(nc)
  if jsonErr != nil {
    jsonError := "{ \"marshal_error\": \"" + jsonErr.Error() + "\"}"
    return []byte(jsonError)
  } 
  return out
}

func (cl ChronicleLogic) GetChronicleByName(chronicleName string) ([]byte, []byte) {
  //metrics

  cd, err := cl.crs.ChronicleDataByName(chronicleName)
  if err != nil {
    sr := StatusResponse{
      ID: "",
      Name: chronicleName,
      Status: "fail",
      StatusCode: 1,
      Error: err.Error(),
    }
    out, jsonErr := json.Marshal(sr)
    if jsonErr != nil {
      jsonError := "{ \"marshal_error\": \"" + jsonErr.Error() + "\", \"original_error\": \"" + err.Error() + "\"}"
      return nil, []byte(jsonError)
    } 
    return nil, out 
  }
  c := Chronicle{
    ID: cd.ID(),
    Name: cd.Name(),
    DefaultLanguage: cd.Language(),
    CreatorIDs: cd.CreatorIDs(),
    HomePageURL: cd.HomePageURL(),
    Discord: cd.Discord(),
    StaffEmail: cd.StaffEmail(),
    CreatedDate: cd.CreationDateTimeUTC(),
    LastUpdated: cd.LastUpdatedDateTimeUTC(),
  }
  jsonData, err := json.Marshal(c)
  if err != nil {
    sr := StatusResponse{
      ID: cd.ID(),
      Name: cd.Name(),
      Status: "fail",
      StatusCode: 1,
      Error: err.Error(),
    }
    out, jsonErr := json.Marshal(sr)
    if jsonErr != nil {
      jsonError := "{ \"marshal_error\": \"" + jsonErr.Error() + "\", \"original_error\": \"" + err.Error() + "\"}"
      return nil, []byte(jsonError)
    } 
    return nil, out 
  }
  return jsonData, nil 
}

func (cl ChronicleLogic) GetChronicleByID(chronicleID string) ([]byte, []byte) {
  //metrics

  cd, err := cl.crs.ChronicleDataByID(chronicleID)
  if err != nil {
    sr := StatusResponse{
      ID: chronicleID,
      Name: "",
      Status: "fail",
      StatusCode: 1,
      Error: err.Error(),
    }
    out, jsonErr := json.Marshal(sr)
    if jsonErr != nil {
      jsonError := "{ \"marshal_error\": \"" + jsonErr.Error() + "\", \"original_error\": \"" + err.Error() + "\"}"
      return nil, []byte(jsonError)
    }
    return nil, out 
  }
  c := Chronicle{
    ID: cd.ID(),
    Name: cd.Name(),
    DefaultLanguage: cd.Language(),
    CreatorIDs: cd.CreatorIDs(), 
    HomePageURL: cd.HomePageURL(),
    Discord: cd.Discord(),
    StaffEmail: cd.StaffEmail(),
    CreatedDate: cd.CreationDateTimeUTC(),
    LastUpdated: cd.LastUpdatedDateTimeUTC(),
  }
  jsonData, err := json.Marshal(c)
  if err != nil {
    sr := StatusResponse{
      ID: cd.ID(),
      Name: cd.Name(),
      Status: "fail",
      StatusCode: 1,
      Error: err.Error(),
    }
    out, jsonErr := json.Marshal(sr)
    if jsonErr != nil {
      jsonError := "{ \"marshal_error\": \"" + jsonErr.Error() + "\", \"original_error\": \"" + err.Error() + "\"}"
      return nil, []byte(jsonError)
    }
    return nil, out
  }
  return jsonData, nil
}

//TODO: implement limits (e.g. DB page size)
func (cl ChronicleLogic) GetChronicleList() ([]byte, error) {
  //metrics

  //get slice
  cdRetSlice, err := cl.crs.ChroniclesAsSlice()
  if err != nil {
    return nil, err
  }
  //compile into json-template structs
  var crl ChronicleList
  for _,v := range cdRetSlice {
    c := Chronicle{
      ID: v.ID(),
      Name: v.Name(),
      DefaultLanguage: v.Language(),
      CreatorIDs: v.CreatorIDs(),
      HomePageURL: v.HomePageURL(),
      Discord: v.Discord(),
      StaffEmail: v.StaffEmail(),
      CreatedDate: v.CreationDateTimeUTC(),
      LastUpdated: v.LastUpdatedDateTimeUTC(),
    }
    crl.Chronicles = append(crl.Chronicles, c) 
  }
  //marshal to json
  jsonData, err := json.Marshal(crl)
  if err != nil {
    return nil, err
  }
  return jsonData, nil
}

//another: shorter list of names with default lang, homepage, discord ID
