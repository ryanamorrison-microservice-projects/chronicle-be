package logic

import(
  "encoding/json"
)

//TODO: implement some form of governance around this
func (cl ChronicleLogic) DeleteChronicleByID(chronicleID string) ([]byte, []byte) {
  
  name, err := cl.crs.DeleteChronicleRecord(chronicleID)
  if err != nil {
    //return nil, err
    sr := StatusResponse{
      ID: chronicleID,
      Name: "",
      Status: "fail",
      StatusCode: 1,
      Error: err.Error(),
    }
    out, jsonErr := json.Marshal(sr)
    if jsonErr != nil {
      jsonError := "{ \"marshal_error\": \"" + jsonErr.Error() + "\", \"original_error\": \"" + err.Error() + "\"}"
      return nil, []byte(jsonError)
    }
    return nil, out
  }

  sr := StatusResponse{
    ID: chronicleID,
    Name: name, 
    Status: "success",
    StatusCode: 0,
    Error: "",
  }

  out, jsonErr := json.Marshal(sr)
  if jsonErr != nil {
    jsonError := "{ \"marshal_error\": \"" + jsonErr.Error() + "\", \"original_error\": \"" + err.Error() + "\"}"
    return nil, []byte(jsonError)
  }

  return out, nil
}
