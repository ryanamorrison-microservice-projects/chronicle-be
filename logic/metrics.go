package logic

import (
  "github.com/prometheus/client_golang/prometheus"
  "github.com/prometheus/client_golang/prometheus/promauto"
)

//metrics counter variables
var(
  newChronicleLogicCounter = promauto.NewCounter(prometheus.CounterOpts{
    Name: "new_chronicle_logic_total",
    Help: "The total number of times NewChronicleLogic() factory function has been called",
  })
  chronicleToJSONCounter = promauto.NewCounter(prometheus.CounterOpts{
    Name: "chronicle_to_json_total",
    Help: "The total number of times ChronicleToJSON() has been called",
  })
  chronicleListToJSONCounter = promauto.NewCounter(prometheus.CounterOpts{
    Name: "chronicle_list_to_json_total",
    Help: "The total number of times ChronicleListToJSON() has been called",
  })
)


