package logic

import(
  "encoding/json"
  "gitlab.com/ryanamorrison-microservice-projects/chronicle-be/data"
)

func (cl ChronicleLogic) UpdateChronicle(jsonData []byte) ([]byte, error) {
  //metric


  var out []byte

  //declare a var of Chronicle to unmarshal into
  var chron Chronicle

  //unmarshal json in bytes to struct
  if err := json.Unmarshal(jsonData, &chron); err != nil {
      return out, err
  }
  //create new chronicleData
  cd, err := data.NewChronicleData(chron.Name, chron.DefaultLanguage, chron.StaffEmail, chron.CreatorIDs...)
  if err != nil {
    return out, err
  }
  if chron.HomePageURL != "" {
    if err := cd.UpdateHomePageURL(chron.HomePageURL); err != nil {
      return out, err
    }
  }
  if chron.Discord != "" {
    if err := cd.UpdateDiscord(chron.Discord); err != nil {
      return out, err
    }
  }
  //update records
  if err := cl.crs.UpdateChronicleRecord(chron.ID, &cd); err != nil {
    //compose an error message to return in json
    return out, err
  }

  //send back a response in json indicating success or failure
  sr := StatusResponse{
    ID: chron.ID,
    Name: chron.Name,
    Status: "successful",
    StatusCode: 0,
  }
  out, err = json.Marshal(sr)
  if err != nil {
    return out, err
  }
  return out, nil
}
