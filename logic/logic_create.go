package logic

import(
  "encoding/json"
  "gitlab.com/ryanamorrison-microservice-projects/chronicle-be/data"
)

type Chronicle struct {
  ID               string    `json:"id"`
  Name             string    `json:"name"`
  DefaultLanguage  string    `json:"default_language"`
  CreatorIDs       []string  `json:"creator_ids"`
  HomePageURL      string    `json:"website_url"`
  Discord          string    `json:"discord"`
  StaffEmail       string    `json:"staff_email"`
  CreatedDate      string    `json:"created_date"`
  LastUpdated      string    `json:"last_updated"`
}

type ChronicleID struct {
  ID string `json:"id"`
}

//unmarshal slice of bytes into a struct
func (cl ChronicleLogic) AddChronicle(postData []byte) ([]byte, []byte) {
  //metric  

  //declare a var of Chronicle to unmarshal into
  var chron Chronicle
  //unmarshal json in bytes to struct
  err := json.Unmarshal(postData, &chron)
  if err != nil {
    sr := StatusResponse{
      ID: "",
      Name: "",
      Status: "fail",
      StatusCode: 1,
      Error: err.Error(),
    }
    out, jsonErr := json.Marshal(sr)
    if jsonErr != nil {
      jsonError := "{ \"marshal_error\": \"" + jsonErr.Error() + "\", \"original_error\": \"" + err.Error() + "\"}"
      return nil, []byte(jsonError)
    }
    return nil, out
  }

  //create new chronicleData 
  cd, err := data.NewChronicleData(chron.Name, chron.DefaultLanguage, chron.StaffEmail, chron.CreatorIDs...)
  if err != nil {
    sr := StatusResponse{
      ID: "",
      Name: chron.Name,
      Status: "fail",
      StatusCode: 1,
      Error: err.Error(),
    }
    out, jsonErr := json.Marshal(sr)
    if jsonErr != nil {
      jsonError := "{ \"marshal_error\": \"" + jsonErr.Error() + "\", \"original_error\": \"" + err.Error() + "\"}"
      return nil, []byte(jsonError)
    }
    return nil, out
  }

  if chron.HomePageURL != "" {
    if err := cd.UpdateHomePageURL(chron.HomePageURL); err != nil {
      sr := StatusResponse{
        ID: cd.ID(),
        Name: cd.Name(),
        Status: "fail",
        StatusCode: 1,
        Error: err.Error(),
      }
      out, jsonErr := json.Marshal(sr)
      if jsonErr != nil {
        jsonError := "{ \"marshal_error\": \"" + jsonErr.Error() + "\", \"original_error\": \"" + err.Error() + "\"}"
        return nil, []byte(jsonError)
      }
      return nil, out
    }
  }

  if chron.Discord != "" {
    if err := cd.UpdateDiscord(chron.Discord); err != nil {
      sr := StatusResponse{
        ID: cd.ID(),
        Name: cd.Name(),
        Status: "fail",
        StatusCode: 1,
        Error: err.Error(),
      }
      out, jsonErr := json.Marshal(sr)
      if jsonErr != nil {
        jsonError := "{ \"marshal_error\": \"" + jsonErr.Error() + "\", \"original_error\": \"" + err.Error() + "\"}"
        return nil, []byte(jsonError)
      }
      return nil, out
    }
  }

  if err = cl.crs.AddChronicleRecord(&cd); err != nil {
    sr := StatusResponse{
      ID: cd.ID(),
      Name: cd.Name(),
      Status: "fail",
      StatusCode: 1,
      Error: err.Error(),
    }
    out, jsonErr := json.Marshal(sr)
    if jsonErr != nil {
      jsonError := "{ \"marshal_error\": \"" + jsonErr.Error() + "\", \"original_error\": \"" + err.Error() + "\"}"
      return nil, []byte(jsonError)
    }
    return nil, out
  }
  sr := StatusResponse{
    ID: cd.ID(),
    Name: cd.Name(),
    Status: "successful",
    StatusCode: 0,
  }
  out, jsonErr := json.Marshal(sr)
  if err != nil {
    jsonError := "{ \"marshal_error\": \"" + jsonErr.Error() + "\"}"
    return nil, []byte(jsonError)
  }
  return out, nil
}
