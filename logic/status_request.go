package logic

import()

type StatusResponse struct {
  ID      string `json:"id"`
  Name    string `json:"name"`
  Status  string `json:"status"`
  StatusCode int `json:"status_code"`
  Error   string `json:"error"`
}

