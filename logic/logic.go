package logic 

import (
  "gitlab.com/ryanamorrison-microservice-projects/chronicle-be/data"
  "gitlab.com/ryanamorrison-microservice-projects/chronicle-be/records"
)

//interface of ChronicleRecords for ChronicleLogic
type ChronicleRecordStore interface {
  ChronicleDataByID(chronicleID string) (*data.ChronicleData, error)
  ChronicleDataByName(chronicleName string) (*data.ChronicleData, error)
  ChroniclesAsSlice() ([]*data.ChronicleData, error)
  AddChronicleRecord(cr records.ChronicleRecord) (error)
  DeleteChronicleRecord(chronicleID string) (string, error) 
  UpdateChronicleRecord(chronicleID string, cr records.ChronicleRecord) (error)
  Len() int
} 

type ChronicleLogic struct {
  ll  LogicLogger
  crs ChronicleRecordStore
  //need something for prometheus here?
}

//factory function
func NewChronicleLogic(crs ChronicleRecordStore, ll LogicLogger) (ChronicleLogic, error) {
  //metric
  newChronicleLogicCounter.Inc()

  return ChronicleLogic{
    ll: ll,
    crs: crs,
  }, nil
} 
