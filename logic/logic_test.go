package logic 

import(
  "encoding/json"
  "github.com/google/uuid"
  "gitlab.com/ryanamorrison-microservice-projects/chronicle-be/logger"
  "gitlab.com/ryanamorrison-microservice-projects/chronicle-be/records"
  "gitlab.com/ryanamorrison-microservice-projects/chronicle-be/shared"
  "gitlab.com/ryanamorrison-microservice-projects/chronicle-be/test_common"
  "testing"
)

//====== test functions ====
func TestLogicChronicle(t *testing.T) {
  //package up everything as it would normally be in main.go
  rl, err := logger.NewLogger("chronicle_records")
  if err != nil {
    t.Error(err)
  }
  crs, err := records.NewChronicleRecords(rl)
  if err != nil {
    t.Error(err)
  }
  ll, err := logger.NewLogger("logic")
  if err != nil {
    t.Error(err)
  }
  l, err := NewChronicleLogic(&crs, ll)
  if err != nil {
    t.Error(err)
  }
  //add test data (test create/search)
  testData := test_common.NewMultipleChronTestData()
  bad := test_common.NewBadChronicleTestData()
  var deleteID string
  var deleteName string
  for _,v := range testData { 
    crIDs := []string{ v.CreatorID, v.PromotedStaffID }
    c := Chronicle{
      Name: v.Name,
      DefaultLanguage: v.Lang,
      CreatorIDs: crIDs,
      HomePageURL: v.HomePageURL,
      Discord: v.Discord,
      StaffEmail: v.StaffEmail,
    }
    //convert to JSON
    jsonRec, err := json.Marshal(c)
    if err != nil {
      t.Error(err)
    } 
    //add
    outJSON, jsonErr := l.AddChronicle(jsonRec)
    if err != nil {
      t.Error(string(jsonErr))
    }
    //test output
    var sr StatusResponse 
    if err := json.Unmarshal(outJSON, &sr); err != nil {
      t.Error(err)
    }
    if err := shared.BadUUID(sr.ID); err != nil {
      t.Error(err)
    }
    //search for valid by ID
    jsonData, jsonErr := l.GetChronicleByID(sr.ID)
    if err != nil {
      t.Error(string(jsonErr))
    }
    var searchChron Chronicle
    if err := json.Unmarshal(jsonData, &searchChron); err != nil {
      t.Error(err)
    }
    if searchChron.ID != sr.ID {
      t.Errorf("Testing error: return ID should have matched search ID. Want: '%s', Got: '%v'", sr.ID, searchChron.ID)
    }
    deleteID = sr.ID
    deleteName = sr.Name
    if searchChron.HomePageURL != v.HomePageURL {
      t.Errorf("Error: return URL (%s) should have matched what was passed to the create function (%s) but did not.", searchChron.HomePageURL, v.HomePageURL)
    }
    if searchChron.Discord != v.Discord {
      t.Errorf("Error: returned Discord string (%s) should have matched what passed to the create function (%s) but did not.", searchChron.Discord, v.Discord)
    }
  }
  //check number of records
  if crs.Len() != len(testData) {
    t.Errorf("Added records should have returned %d but instead returned %d.", len(testData), crs.Len())
  }
  //ask for json list and unmarshal
  jsonList, err := l.GetChronicleList()
  if err != nil {
    t.Error(err)
  }
  var unmarshaledList ChronicleList
  if err := json.Unmarshal(jsonList, &unmarshaledList); err != nil {
    t.Error(err)
  }
  //name search returned records
  hits := 0
  for _,v := range testData {
    for _,rec := range unmarshaledList.Chronicles {
      if v.Name == rec.Name {
        hits++
      }
    }
  }
  if hits != len(testData) {
    t.Errorf("Returned records were searched for each of the test names, the number of test records (%d) should match the search hits (%d) but they did not.", hits, len(testData))
  }
  //ByName search good v. bad  
  searchGoodData, searchErr := l.GetChronicleByName(deleteName)
  if searchGoodData == nil {
    t.Errorf("Did not get a hit back on '%s' like expected. Error (if populated): %s.", deleteName, string(searchErr)) 
  } 
  searchBadData, searchErr := l.GetChronicleByName(bad.Name) 
  if searchBadData != nil {
    t.Errorf("Got back a hit on '%s' which was unexpected.  Error (if populated): %s.", bad.Name, string(searchErr))
  }
  //search for invalid by ID
  badID := uuid.New().String() 
  searchBadData, searchIdErr := l.GetChronicleByID(badID) 
  if searchBadData != nil {
    t.Errorf("Got back a hit on '%s' which was unexpected.  Error (if populated): %s.", badID, searchIdErr)
  }
  //delete test
  var sr StatusResponse
  jsonResp, jsonErr := l.DeleteChronicleByID(deleteID) 
  if jsonErr != nil {
    t.Error(string(jsonErr))
  }
  if err := json.Unmarshal(jsonResp, &sr); err != nil {
    t.Error(err)
  }
  //verify deletion
  if sr.Name != deleteName {
    t.Errorf("Was expecting '%s' back as the name deleted, got '%s' instead.", deleteName, sr.Name)
  }
  jsonData, jsonErr2 := l.GetChronicleByID(sr.ID)
  if jsonErr2 == nil {
    t.Errorf("Should have received no hits on search for a deleted records.  Got: %s", jsonData) 
  }
  jsonData = l.NumberOfChronicles()
  var nc Chronicles
  if err = json.Unmarshal(jsonData, &nc); err != nil {
    t.Error(err)
  }
  if nc.NumberOfChronicles != (len(testData) - 1) {
    t.Errorf("Wrong number of records.  Want: %d, Got: %d.", (len(testData) - 1), nc.NumberOfChronicles)
  }
}
