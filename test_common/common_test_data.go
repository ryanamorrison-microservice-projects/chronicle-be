package test_common 

//=====test data ===================================
type ChronicleTestData struct {
  Name             string
  UpdatedName      string
  Lang             string
  CreatorID        string
  PromotedStaffID  string
  PlayerID         string
  HomePageURL      string
  UpdatedHomePage  string
  StaffEmail       string
  UpdatedEmail     string
  Discord          string
  UpdatedDiscord   string
}

func NewChronicleTestData() (ChronicleTestData) {
  var want = ChronicleTestData{
    Name:            "Area by night Game",
    UpdatedName:     "Area by Night Game",
    Lang:            "eng",
    CreatorID:       "f7c6a14b-c4b3-4f8c-bfb7-d3f78f0ce193",
    PromotedStaffID: "379b1dbd-eef8-4613-8ff4-5960f6c2c292",
    PlayerID:        "a7799f40-b733-48f5-8dd5-fc0760e889aa",
    HomePageURL:     "http://abn.hoster.com",
    UpdatedHomePage: "http://areabynight.example.org",
    StaffEmail:      "my-mail@commercial-mail.com",
    UpdatedEmail:    "abn-staff@commercial-mail.com",
    Discord:         "123456789012345678",
    UpdatedDiscord:  "098765432109876543",
  }
  return want
}

func NewBadChronicleTestData() (ChronicleTestData){
  var bad = ChronicleTestData{
    Name:        "AA",
    Lang:        "zzzz",
    CreatorID:   "Um, I'm not sure.",
    HomePageURL: "foo",
    StaffEmail:  "hahhaha.com",
  }
  return bad
}

func NewMultipleChronTestData() ([]ChronicleTestData) {
  var ctd ChronicleTestData
  var mctd []ChronicleTestData
  ctd = NewChronicleTestData()
  mctd = append(mctd, ctd)
  ctd = ChronicleTestData{
    Name:            "Under a red Moon",
    UpdatedName:     "Under a Red Moon",
    Lang:            "eng",
    CreatorID:       "dc086c28-9ba7-4997-9830-178ce0429bae",
    PromotedStaffID: "4079dffb-9005-4d72-af42-aaa4fac82db5",
    PlayerID:        "7e5c2de1-f6b6-4cdc-a974-7c99545194a7",
    HomePageURL:     "http://urm.hoster.com",
    UpdatedHomePage: "http://underaredmoon.example.org",
    StaffEmail:      "urm@commercial-mail.com",
    UpdatedEmail:    "urm-staff@commercial-mail.com",
    Discord:         "234567890123456781",
    UpdatedDiscord:  "987654321098765432",
  }
  mctd = append(mctd, ctd)
  ctd = ChronicleTestData{
    Name:            "Área à Noite",
    UpdatedName:     "Área à noite",
    Lang:            "por",
    CreatorID:       "11899657-5810-4442-9f7c-f7626766a89d",
    PromotedStaffID: "14aad2b4-8774-4f04-9748-0fb12b7e1be4",
    PlayerID:        "1441e495-b66c-4d8b-8bdb-682b82071c7c",
    HomePageURL:     "http://urm.hoster.com",
    UpdatedHomePage: "http://underaredmoon.example.org",
    StaffEmail:      "aan@commercial-mail.com",
    UpdatedEmail:    "aan-staff@commercial-mail.com",
    Discord:         "345678901234567812",
    UpdatedDiscord:  "876543210987654321",
  }
  mctd = append(mctd, ctd)
  return mctd 
}

