package controller 

import (
  "github.com/prometheus/client_golang/prometheus"
  "github.com/prometheus/client_golang/prometheus/promauto"
)

//metrics counter variables
var(
  getChronicleGoodIDCounter = promauto.NewCounter(prometheus.CounterOpts{
    Name: "get_chronicle_good_id",
    Help: "The total number of times a successful chronicle record has been returned",
  })
  getChronicleBadIDCounter = promauto.NewCounter(prometheus.CounterOpts{
    Name: "get_chronicle_bad_id",
    Help: "The total number of times a chroncile lookup failed due to a bad ID being supplied",
  })
)


