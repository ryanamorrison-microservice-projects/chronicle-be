package controller 

//interface of Logger for api controller
type ChronicleLogger interface {
  ErrorLogger(message string) (error)
  WarnLogger(message string) (error)
  InfoLogger(message string) (error)
}

