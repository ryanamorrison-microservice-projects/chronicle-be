package controller 

import ()

//interface of ChronicleLogic for api controller 
type ChronicleLogic interface {
  GetChronicleByID(chronicleID string) ([]byte, error)
  GetChronicleByName(chronicleID string) ([]byte, error)
  GetChronicleList() ([]byte, error)
  AddChronicle(jsonData []byte) ([]byte, error)
  DeleteChronicle(chronicleID string) ([]byte, error)
  UpdateChronicle(jsonData []byte) ([]byte, error)
}

//api
type Controller struct {
  cl     ChronicleLogger
  logic  ChronicleLogic
}

//factory function to create a new controller
func NewController(cl ChronicleLogger, logic ChronicleLogic) (Controller) {
  return Controller {
    cl:    cl,
    logic: logic,
  }
}
