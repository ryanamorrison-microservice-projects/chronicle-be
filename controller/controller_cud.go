package controller

import(
  "fmt"
  "github.com/gorilla/mux"
  "io"
  "net/http"
)

func (ctrl Controller) CreateChronicle(w http.ResponseWriter, r *http.Request) {
  //metric


  body, err := io.ReadAll(r.Body)
  if err != nil {
    http.Error(w, fmt.Sprintf("Error parsing body: %v", err), http.StatusInternalServerError)
    //log
    return
  }
  statusData, err := ctrl.logic.AddChronicle(body) 
  if err != nil {
    http.Error(w, fmt.Sprintf("Error adding chronicle: %v", err), http.StatusInternalServerError)
    //log
    return
  }
  w.Write(statusData)
}

func (ctrl Controller) UpdateChronicle(w http.ResponseWriter, r *http.Request) {
  //metric


  body, err := io.ReadAll(r.Body)
  if err != nil {
    http.Error(w, fmt.Sprintf("Error parsing body: %v", err), http.StatusInternalServerError)
    //log
    return
  }
  statusData, err := ctrl.logic.UpdateChronicle(body)
  if err != nil {
    http.Error(w, fmt.Sprintf("Error updating chronicle: %v", err), http.StatusInternalServerError)
    //log
    return
  }
  w.Write(statusData)
  
}

//TODO: governance needs to be added to this (e.g., a vote to disband, rules on how that can occur)
func (ctrl Controller) DeleteChronicle(w http.ResponseWriter, r *http.Request) {
  //metric


  params := mux.Vars(r)
  statusData, err := ctrl.logic.DeleteChronicle(params["id"])
  if err != nil {
    http.Error(w, fmt.Sprintf("Error deleting chronicle: %v", err), http.StatusInternalServerError)
    //log
    return
  }
  w.Write(statusData)
}
