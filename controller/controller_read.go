package controller 

import (
  "net/http"
)

//get chronicle by ChronicleID
func (ctrl Controller) ChronicleByID(w http.ResponseWriter, r *http.Request) {
  //metric

  
  chronicleID := r.URL.Query().Get("id")  

  chronicleJSON, err := ctrl.logic.GetChronicleByID(chronicleID)
  if err != nil {
    //getChronicleBadIDCounter.Inc()
    ctrl.cl.ErrorLogger(err.Error())

    w.WriteHeader(http.StatusBadRequest)
    w.Write([]byte(err.Error()))
    return
  }

  //getChronicleGoodIDCounter.Inc() 
  w.Write(chronicleJSON)
}



/*
//get all chronicles 
func (c Controller) ChroniclesByUserID(w http.ResponseWriter, r *http.Request) {
  
}
*/
