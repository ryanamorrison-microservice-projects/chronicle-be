package main

import(
  "github.com/gorilla/mux"
  "net/http"
  "time"
)



func main() {

  //path router (using Gorilla)
  router := mux.NewRouter()

  //handler functions
  //router.HandleFunc("/path", funcName or closure)

  //server
  serv := http.Server{
    Addr:         ":",
    ReadTimeout:  30 * time.Second,
    WriteTimeout: 90 * time.Second,
    IdleTimeout:  90 * time.Second,
    Handler:      router,
  }

  //start server and check for errors  
  err := serv.ListenAndServe()
  if err != nil {
    if err != http.ErrServerClosed {
      panic(err)
    }
  }
}
