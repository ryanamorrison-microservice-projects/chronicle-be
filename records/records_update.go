package records

import(
  "fmt"
)

//TODO: missing validation from staffID
func (crs *ChronicleRecords) UpdateChronicleRecord(chronicleID string, cr ChronicleRecord) (error) {
  //metrics
  updateChronicleRecordCounter.Inc()

  if crs.chronicleRecStore == nil {
    return fmt.Errorf("ChronicleRecords not initialized, use factory function to create.")
  }

  _, ok := crs.chronicleRecStore[chronicleID]
  if !ok {
    return fmt.Errorf("ChronicleData (%s) not found in update search.", chronicleID)
  }
  if err := crs.chronicleRecStore[chronicleID].UpdateName(cr.Name()); err != nil {
    return err
  }
  if err := crs.chronicleRecStore[chronicleID].UpdateHomePageURL(cr.HomePageURL()); err != nil {
    return err
  }
  if err := crs.chronicleRecStore[chronicleID].UpdateDiscord(cr.Discord()); err != nil {
    return err
  }
  if err := crs.chronicleRecStore[chronicleID].UpdateStaffEmail(cr.StaffEmail()); err != nil {
    return err
  }

  //crs.rl.InfoLogger(fmt.Sprintf("end updating ChronicleRecord (%s)", chronicleID))
  return nil
}
