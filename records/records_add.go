package records

import(
  "fmt"
)

//func AddChronicleData adds a record to the datastore
func (crs *ChronicleRecords) AddChronicleRecord(cr ChronicleRecord) (error) {
  //metrics
  addChronicleRecordCounter.Inc()

  if crs.chronicleRecStore == nil {
    return fmt.Errorf("ChronicleRecords not initialized, use factory function to create.")
  } 

  //check for an existing record by Name
  cd, _ := crs.ChronicleDataByName(cr.Name())
  if cd != nil {
    return fmt.Errorf("Error: existing record (%s [%s]) found. Nothing to add.", cr.Name(), cr.ID())
  }

  //check for an existing record by ID
  cd, _ = crs.ChronicleDataByID(cr.ID())
  if cd != nil {
    return fmt.Errorf("Error: existing record (%s [%s]) found. Nothing to add.", cr.ID(), cr.Name())
  }

  crs.chronicleRecStore[cr.ID()] = cr
  crs.rl.InfoLogger(fmt.Sprintf("add ChronicleRecord %s to ChronicleRecords", cr.ID()))
  return nil
}
