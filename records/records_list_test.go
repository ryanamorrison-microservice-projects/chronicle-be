package records

import(
  "gitlab.com/ryanamorrison-microservice-projects/chronicle-be/data"
  "gitlab.com/ryanamorrison-microservice-projects/chronicle-be/logger"
  "gitlab.com/ryanamorrison-microservice-projects/chronicle-be/test_common"
  "testing"
)

func TestGetRecordListAndVerify(t *testing.T) {
  //test empty datastore first, should return error

   //create the CRS
  reclog, err := logger.NewLogger("chronicle_records")
  if err != nil {
    t.Error(err)
  }
  crs, err := NewChronicleRecords(reclog)
  if err != nil {
    t.Error(err)
  }
  //add test data to search on
  testData := test_common.NewMultipleChronTestData()
  for _,v := range testData {
    cd, err := data.NewChronicleData(v.Name, v.Lang, v.StaffEmail, v.CreatorID)
    if err != nil {
      t.Error(err)
    }
    if err = crs.AddChronicleRecord(&cd); err != nil {
      t.Error(err)
    }
  } 
  if crs.Len() != len(testData) {
    t.Errorf("Added records should have returned %d but instead returned %d.", len(testData), crs.Len())
  }
  cdRetSlice, err := crs.ChroniclesAsSlice()
  if err != nil {
    t.Error(err)
  }
  if len(cdRetSlice) != len(testData) {
    t.Errorf("Returned records should have returned %d but instead returned %d.", len(testData), len(cdRetSlice))
  }
  hits := 0
  for _,v := range cdRetSlice {
    for _,rec := range testData {
      if v.Name() == rec.Name {
        hits++
      }
    }
  }
  if hits != len(testData) {
    t.Errorf("Returned records were searched for each of the test names, the number of test records (%d) should match the search hits (%d) but they did not.", hits, len(testData))
  }
}
