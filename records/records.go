package records 

import()

//an interface to abstract the chronicle_data representation
//note that it does not contain every CD method as CRS doesn't need them all
type ChronicleRecord interface {
  ID() (string)                                 //used in Add and Delete
  Name() (string)                               //used in search by Name
  HomePageURL() (string)                        //used by Update
  Discord() (string)
  StaffEmail() (string)
  UpdateName(chronicleName string) (error)
  UpdateHomePageURL(homePageURL string) (error)
  UpdateDiscord(discordID string) (error)
  UpdateStaffEmail(staffEmail string) (error)
}

//MVP: a fake database (for now)
type ChronicleRecords struct {
  chronicleRecStore  map[string]ChronicleRecord
  rl                 RecordLogger
}

//factory function to create a new datastore
func NewChronicleRecords(rl RecordLogger) (ChronicleRecords, error) {
  //metrics
  newChronicleRecordsCounter.Inc()

  return ChronicleRecords{
    chronicleRecStore: make(map[string]ChronicleRecord),
    rl: rl,   
  }, nil
}

