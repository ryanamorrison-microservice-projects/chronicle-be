package records

import(
  "fmt"
  "gitlab.com/ryanamorrison-microservice-projects/chronicle-be/data"
)

func (crs ChronicleRecords) Len() int {
  return len(crs.chronicleRecStore)
}

func (crs ChronicleRecords) ChroniclesAsSlice() ([]*data.ChronicleData, error) {
  //metrics

  var cdSlice []*data.ChronicleData
  if crs.chronicleRecStore == nil {
    return cdSlice, fmt.Errorf("ChronicleRecords not initialized, use factory function to create.")
  }

  for _,v := range crs.chronicleRecStore {
    cd, ok := v.(*data.ChronicleData)
    if !ok {
      return cdSlice, fmt.Errorf("Unexpected type stored in ChronicleRecordStore: %v", v)
    }
    cdSlice = append(cdSlice, cd)
  } 
  return cdSlice, nil
}
