package records

import(
  "fmt"
)

//TODO: missing validation from staffID
func (crs *ChronicleRecords) DeleteChronicleRecord(chronicleID string) (string, error) {
  //metrics
  deleteChronicleRecordCounter.Inc()

  if crs.chronicleRecStore == nil {
    warnMsg := fmt.Sprintf("No ChronicleRecords found. Nothing to do.")
    return "", fmt.Errorf(warnMsg)
  }

  cd, ok := crs.chronicleRecStore[chronicleID]
  if !ok {
    warnMsg := fmt.Sprintf("ChronicleData (%s) not found in delete search.", chronicleID)
    crs.rl.WarnLogger(warnMsg)
    return "", fmt.Errorf(warnMsg)
  }
  name := cd.Name()

  delete(crs.chronicleRecStore, chronicleID)
  crs.rl.InfoLogger(fmt.Sprintf("delete ChronicleRecord %s from ChronicleRecords", chronicleID))
  return name, nil
}

