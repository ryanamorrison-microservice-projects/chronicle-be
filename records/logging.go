package records

type RecordLogger interface {
  ErrorLogger(message string) (error)
  WarnLogger(message string) (error)
  InfoLogger(message string) (error)
}

