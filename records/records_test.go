package records 

import (
  "github.com/google/uuid"
  "gitlab.com/ryanamorrison-microservice-projects/chronicle-be/data"
  "gitlab.com/ryanamorrison-microservice-projects/chronicle-be/logger"
  "gitlab.com/ryanamorrison-microservice-projects/chronicle-be/test_common"
  "testing"
)

func TestChronicleRecordsFactoryFunction(t *testing.T) {
  reclog, err := logger.NewLogger("chronicle_records")
  if err != nil {
    t.Error(err)
  }
  crs, err := NewChronicleRecords(reclog)
  if err != nil {
    t.Error(err)
  }
  if num := crs.Len(); num != 0 {
    t.Errorf("Testing error: number of records (%d) did not match in factory function.\n", num)
  }
}

func TestAddChronicleDataToChronicleRecords(t *testing.T) {
  want := test_common.NewChronicleTestData()
  reclog, err := logger.NewLogger("chronicle_records")
  if err != nil {
    t.Error(err)
  }
  crs, err := NewChronicleRecords(reclog)
  if err != nil {
    t.Error(err)
  }
  cd, err := data.NewChronicleData(want.Name, want.Lang, want.StaffEmail, want.CreatorID)
  if err != nil {
    t.Error(err)
  }
  if err = crs.AddChronicleRecord(&cd); err != nil {
    t.Error(err)
  }
}

func TestAddChronicleDataToEmptyChronicleRecords(t *testing.T) {
  want := test_common.NewChronicleTestData()
  var crs ChronicleRecords
  cd, err := data.NewChronicleData(want.Name, want.Lang, want.StaffEmail, want.CreatorID)
  if err != nil {
    t.Error(err)
  }
  err = crs.AddChronicleRecord(&cd)
  if err == nil {
    t.Error(err)
  }
}

func TestChronicleDataByName(t *testing.T) {
  want := test_common.NewChronicleTestData()
  reclog, err := logger.NewLogger("chronicle_records")
  if err != nil {
    t.Error(err)
  }
  crs, err := NewChronicleRecords(reclog)
  if err != nil {
    t.Error(err)
  } 
  _, err = crs.ChronicleDataByName(want.Name)
  if err == nil {
    t.Error(err)
  }
  cd, err := data.NewChronicleData(want.Name, want.Lang, want.StaffEmail, want.CreatorID)
  if err != nil {
    t.Error(err)
  }
  err = crs.AddChronicleRecord(&cd)
  if err != nil {
    t.Error(err)
  }
  if num := crs.Len(); num != 1 {
    t.Errorf("Testing error: number of records (%d) did not match in factory function.\n", num)
  }
  _, err = crs.ChronicleDataByName(want.Name)
  if err != nil {
    t.Error(err)
  }
}

func TestAddDuplicateRecords(t *testing.T) {
  want := test_common.NewChronicleTestData()
  reclog, err := logger.NewLogger("chronicle_records")
  if err != nil {
    t.Error(err)
  }
  crs, err := NewChronicleRecords(reclog)
  if err != nil {
    t.Error(err)
  }
  cd1, err := data.NewChronicleData(want.Name, want.Lang, want.StaffEmail, want.CreatorID)
  if err != nil {
    t.Error(err)
  }
  //cd2 will be a duplicate but will have a different UUID
  cd2, err := data.NewChronicleData(want.Name, want.Lang, want.StaffEmail, want.CreatorID)
  if err != nil {
    t.Error(err)
  }
  if err := crs.AddChronicleRecord(&cd1); err != nil {
    t.Error(err)
  }
  if err := crs.AddChronicleRecord(&cd2); err == nil {
    t.Errorf("Testing error: AddChronicleData should have returned an error while adding a duplicate record but did not.")
  }
  if num := crs.Len(); num != 1 {
    t.Errorf("Testing error: number of records (%d) did not match in factory function.\n", num)
  }
}

func TestDeleteChroniclesInChronicleRecords(t *testing.T) {
  var ecrs ChronicleRecords
  if _, err := ecrs.DeleteChronicleRecord(uuid.New().String()); err == nil {
    t.Errorf("Testing error: empty datastore did not raise an error.\n")
  }
  want := test_common.NewChronicleTestData()
  reclog, err := logger.NewLogger("chronicle_records")
  if err != nil {
    t.Error(err)
  }
  crs, err := NewChronicleRecords(reclog)
  if err != nil {
    t.Error(err)
  }
  cd, err := data.NewChronicleData(want.Name, want.Lang, want.StaffEmail, want.CreatorID)
  if err != nil {
    t.Error(err)
  }
  if err := crs.AddChronicleRecord(&cd); err != nil {
    t.Error(err)
  }
  if _, err := crs.DeleteChronicleRecord(cd.ID()); err != nil {
    t.Error(err)
  }
  if num := crs.Len(); num != 0 {
    t.Errorf("Testing error: number of records (%d) were not correct after testing DeleteChronicleRecord.\n", num)
  }
  if _, err := crs.DeleteChronicleRecord(uuid.New().String()); err == nil {
    t.Errorf("Testing error: bogus ID did not raise an error in DeleteChronicleRecord.")
  }
  if err := crs.AddChronicleRecord(&cd); err != nil {
    t.Error(err)
  }
  if _, err := crs.DeleteChronicleRecord(uuid.New().String()); err == nil {
    t.Errorf("Testing error: bogus ID did not raise an error in DeleteChronicleRecord.")
  }
  
}

func TestUpdateRecords(t *testing.T) {
  want := test_common.NewChronicleTestData()
  cd, err := data.NewChronicleData(want.Name, want.Lang, want.StaffEmail, want.CreatorID)
  if err != nil {
    t.Error(err)
  }
  var ecrs ChronicleRecords
  if err := ecrs.UpdateChronicleRecord(cd.ID(), &cd); err == nil {
    t.Errorf("Testing error: empty datastore did not raise an error.\n")
  }
  reclog, err := logger.NewLogger("chronicle_records")
  if err != nil {
    t.Error(err)
  }
  crs, err := NewChronicleRecords(reclog)
  if err != nil {
    t.Error(err)
  }
  if err := crs.AddChronicleRecord(&cd); err != nil {
    t.Error(err)
  }
  updateCD, err := data.NewChronicleData(want.UpdatedName, want.Lang, want.UpdatedEmail, want.CreatorID)
  if err != nil {
    t.Error(err)
  }
  if err = crs.UpdateChronicleRecord(cd.ID(), &updateCD); err != nil {
    t.Error(err)
  }
  gotCD, err := crs.ChronicleDataByID(cd.ID())
  if err != nil {
    t.Error(err)
  }
  if gotCD.StaffEmail() != want.UpdatedEmail {    
    t.Errorf("Chronicle (%s) should have an updated staff email.  Want: %s, Got: %s.  There are %d records.", cd.ID(), want.UpdatedEmail, gotCD.StaffEmail(), ecrs.Len()) 
  }
}
