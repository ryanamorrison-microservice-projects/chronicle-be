package records

import(
  "github.com/prometheus/client_golang/prometheus"
  "github.com/prometheus/client_golang/prometheus/promauto"
)

//metrics counter variables
var(
  makeRecordStoreCounter = promauto.NewCounter(prometheus.CounterOpts{
    Name: "make_record_store_total",
    Help: "The total number of times makeRecordStore() has been called",
  })
  chronicleDataByIDCounter = promauto.NewCounter(prometheus.CounterOpts{
    Name: "chronicle_data_by_id_total",
    Help: "the total number of times ChronicleDataByIDCounter() has been called",
  })
  chronicleDataByNameCounter = promauto.NewCounter(prometheus.CounterOpts{
    Name: "chronicle_data_by_name_total",
    Help: "the total number of times ChronicleDataByName() has been called",
  })
  chronicleDataListByStaffIDCounter = promauto.NewCounter(prometheus.CounterOpts{
    Name: "chronicle_data_list_by_staff_id_total",
    Help: "the total number of times ChronicleDataListByStaffID() has been called",
  })
  chronicleDataListByPlayerIDCounter = promauto.NewCounter(prometheus.CounterOpts{
    Name: "chronicle_data_list_by_player_id_total",
    Help: "the total number of times ChronicleDataListByPlayerID() has been called",
  })
  addChronicleRecordCounter = promauto.NewCounter(prometheus.CounterOpts{
    Name: "add_chronicle_record_total",
    Help: "the total number of times AddChronicleRecord() has been called",
  })
  deleteChronicleRecordCounter = promauto.NewCounter(prometheus.CounterOpts{
    Name: "delete_chronicle_record_total",
    Help: "the total number of times DeleteChronicleRecord() has been called",
  })
  updateChronicleRecordCounter = promauto.NewCounter(prometheus.CounterOpts{
    Name: "update_chronicle_record_total",
    Help: "the total number of times UpdateChronicleRecord() has been called",
  })
  newChronicleRecordsCounter = promauto.NewCounter(prometheus.CounterOpts{
    Name: "new_chronicle_records_total",
    Help: "the total number of times NewChronicleRecords() factory function has been called",
  })
)

