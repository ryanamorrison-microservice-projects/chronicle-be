package records

import(
  "fmt"
  "strings"
  "gitlab.com/ryanamorrison-microservice-projects/chronicle-be/data"
)

//func ChronicleDataByID is the default search, retrieves a chronicle record
//by the map key from the database
func (crs ChronicleRecords) ChronicleDataByID(chronicleID string) (*data.ChronicleData, error) {
  //metrics
  chronicleDataByIDCounter.Inc()

  if crs.chronicleRecStore == nil {
    return nil, fmt.Errorf("ChronicleRecords not initialized, use factory function to create.")
  }
  cr, ok := crs.chronicleRecStore[chronicleID]
  if !ok {
    return nil, fmt.Errorf("ChronicleData (%s) not found.", chronicleID)
  }
  cd, ok := cr.(*data.ChronicleData)
  if !ok {
    errorMsg := fmt.Sprintf("unexpected type for %v", cr)
    crs.rl.ErrorLogger(errorMsg)
    return nil, fmt.Errorf(errorMsg)
  }
  return cd, nil
}

//MVP: func ChronicleDataByName retrieves data by slow scanning
func (crs ChronicleRecords) ChronicleDataByName(chronicleName string) (*data.ChronicleData, error) {
  //metrics
  chronicleDataByNameCounter.Inc()

  if crs.chronicleRecStore == nil {
    return nil, fmt.Errorf("ChronicleRecords not initialized, use factory function to create.")
  }

  searchStr := strings.ToLower(chronicleName)
  for _, cr := range crs.chronicleRecStore {
    compareStr := strings.ToLower(cr.Name())
    if searchStr == compareStr {
      cd, ok := cr.(*data.ChronicleData)
      if !ok {
        errorMsg := fmt.Sprintf("unexpected type for %v", cr)
        crs.rl.ErrorLogger(errorMsg)
        return nil, fmt.Errorf(errorMsg)
      }
      return cd, nil
    }
  }
  return nil, fmt.Errorf("search (%s) not found", chronicleName)
}

