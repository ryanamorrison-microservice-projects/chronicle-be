package data

import (
	"github.com/google/uuid"
	"testing"
  "time"
  "gitlab.com/ryanamorrison-microservice-projects/chronicle-be/test_common"
)
  
//==============tests========================================
func TestChroniceDataFactoryFunction(t *testing.T) {
  want := test_common.NewChronicleTestData()
	cd, err := NewChronicleData(want.Name, want.Lang, want.StaffEmail, want.CreatorID)
  //error returned from function
	if err != nil {
		t.Error(err)
	}
  //valid UUID
	if _, err := uuid.Parse(cd.ID()); err != nil {
		t.Error(err)
	}
  //chronicle name matches
	if got := cd.Name(); want.Name != got {
		t.Errorf("Testing Error, want %s, but got %s\n", want.Name, got)
	}
  //language matches
	if got := cd.Language(); want.Lang != got {
		t.Errorf("Testing Error, want %s, but got %s\n", want.Lang, got)
	}
  //staffID matches
  found := false
  creatorIDs := cd.CreatorIDs()
  for _,v := range creatorIDs {
    if v == want.CreatorID {
      found = true
    } 
  }
  if found == false {
		t.Errorf("Testing Error, failed to find %s in creatorIDs slice.", want.CreatorID)
  }
  //email matches
  if got := cd.StaffEmail(); want.StaffEmail != got {
    t.Errorf("Testing Error, want %s, but got %s\n", want.StaffEmail, got)
  }
}
func TestChronicleDataUpdateFunctions(t *testing.T) {
  want := test_common.NewChronicleTestData()
  //update chronicle name
	cd, err := NewChronicleData(want.Name, want.Lang, want.StaffEmail, want.CreatorID, want.PromotedStaffID)
  //check for more than 1 staffID matches
  found1 := false
  found2 := false
  creatorIDs := cd.CreatorIDs()
  for _,v := range creatorIDs {
    if v == want.CreatorID {
      found1 = true
    }
    if v == want.PromotedStaffID {
      found2 = true
    }
  }
  if found1 == false || found2 == false {
    t.Errorf("Testing Error, failed to find %s (%t) and/or %s (%t) in creatorIDs slice.", want.CreatorID, found1, want.PromotedStaffID, found2)
  }

  old_date := cd.LastUpdatedDateTimeUTC()
  //ensure that it is possible to have a later date in the test (IOW: no false positives because
  //the code is executing in ms)
  time.Sleep(1 * time.Second) 
	if err != nil {
		t.Error(err)
	}
	_ = cd.UpdateName(want.UpdatedName)
	if got := cd.Name(); want.UpdatedName != got {
		t.Errorf("Testing Error, want %s, but got %s\n", want.UpdatedName, got)
	}
  //update home page
  _ = cd.UpdateHomePageURL(want.UpdatedHomePage)
  if got := cd.HomePageURL(); want.UpdatedHomePage != got {
    t.Errorf("Testing Error, want %s, but got: %s", want.UpdatedHomePage, got)
  } 
  //update staff email address
  _ = cd.UpdateStaffEmail(want.UpdatedEmail)
  if got := cd.StaffEmail(); want.UpdatedEmail != got {
    t.Errorf("Testing Error, want %s, but got: %s", want.UpdatedEmail, got)
  }
  //check the updated time
  if new_date := cd.LastUpdatedDateTimeUTC(); new_date == old_date {
    t.Errorf("Testing Error, dates match: old is %s, new is %s", old_date, new_date)
  }
}

func TestChronicleErrorHandling(t *testing.T) {
  want := test_common.NewChronicleTestData()
  bad := test_common.NewBadChronicleTestData()
	cd, err := NewChronicleData(want.Name, want.Lang, want.CreatorID, want.HomePageURL, want.StaffEmail)
  err = cd.UpdateName(bad.Name)
	if err == nil {
		t.Errorf("Expecting error message for a chronicle name with a name that was too small, but got nil\n")
  }
  /*
	//add language validation here 
	_, err := NewChronicleData(want.Name, bad.Lang, want.OwnerId, want.HomePageURL, want.StaffEmail)
	if err == nil {
		t.Errorf("Expecting error message for passing in a bad language code, but got nil\n")
	} else {
		fmt.Printf("Bad chronicle language test passed.")
	}
  */
  err = cd.UpdateHomePageURL(bad.HomePageURL)
  if err == nil {
		t.Errorf("Expecting error message for updating home page with bad url, but got nil.\n")
  }
  err = cd.UpdateStaffEmail(bad.StaffEmail)
  if err == nil {
    t.Errorf("Expecting error message for updating email address with bad address, but got nil.\n")
  }
}
