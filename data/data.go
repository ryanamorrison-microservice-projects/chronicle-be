package data 

import(
  "github.com/google/uuid"
  "time"
  "gitlab.com/ryanamorrison-microservice-projects/chronicle-be/shared"
)

//chronicle data structure
type ChronicleData struct {
  id               string
  name             string
  defaultLang      string
  creatorIDs       []string  //a historical record
  homePageURL      string
  chronicleDiscord string
  staffEmail       string
  createdDate      time.Time
  lastUpdated      time.Time
}

//factory function, primarily for ChronicleRecord
func NewChronicleData(chronicleName, language, staffEmail string, creatorIDs ...string) (ChronicleData, error) {
  var cd ChronicleData
  if len(creatorIDs) > 0 {
    for _,v := range creatorIDs {
      err := shared.BadUUID(v)
      if err != nil {
        return cd, err
      }
    }
  }
  if err := shared.ShortName(chronicleName); err != nil {
    return cd, err
  }
  if err := shared.BadEmail(staffEmail); err != nil {
    return cd, err
  }
  //TODO: need language validation here (requires another microservice)
  newid := uuid.New()
  return ChronicleData{
    id: newid.String(),
    name: chronicleName,
    defaultLang: language,
    creatorIDs: creatorIDs, 
    homePageURL: "",
    chronicleDiscord: "",
    staffEmail: staffEmail,
    createdDate: time.Now().UTC(),
    lastUpdated: time.Now().UTC(),
  }, nil
}

//func Name returns the name of a the chronicle
func (cd ChronicleData) Name() string {
  return cd.name
}

//func UpdateName allows a name of a chronicle to be changed
func (cd *ChronicleData) UpdateName(chronicleName string) (error) {
  if err := shared.ShortName(chronicleName); err != nil {
    return err
  }
  cd.name = chronicleName
  cd.lastUpdated = time.Now().UTC()
  return nil
}

//func ID returns the UUID generated when the chronicle was created
func (cd ChronicleData) ID() string {
  return cd.id
}

//func Language returns the language setting for the chronicle
func (cd ChronicleData) Language() string {
  return cd.defaultLang
}

//func CreatorID returns the UUID of the person in the system who created the chronicle
func (cd ChronicleData) CreatorIDs() []string {
  return cd.creatorIDs
}

//func HomePageURL returns the home page URL for the chronicle
func (cd ChronicleData) HomePageURL() string {
  return cd.homePageURL
}

//func UpdateHomePageURL updates a valid URL and if the URL is invalid it returns an error
func (cd *ChronicleData) UpdateHomePageURL(homePageURL string) (error) {
  if err := shared.BadURL(homePageURL); err != nil {
    return err
  }
  cd.homePageURL = homePageURL
  return nil
}

func (cd *ChronicleData) UpdateDiscord(discordID string) (error) {
  cd.chronicleDiscord = discordID
  return nil
}

func (cd *ChronicleData) Discord() (string) {
  return cd.chronicleDiscord
}

//func StaffEmailAddress returns the email address listed as the staff email address on the chronicle home page
func (cd ChronicleData) StaffEmail() string {
  return cd.staffEmail
}

//func UpdateStaffEmailAddress updates a valid email address and if the address is invalid returns an error
func (cd *ChronicleData) UpdateStaffEmail(staffEmail string) (error) {
  if err := shared.BadEmail(staffEmail); err != nil {
    return err
  }
  cd.staffEmail = staffEmail
  return nil
}

//func CreationDateTimeUTC returns the creation date and time (UTC) recorded for the chronicle in this system
func (cd ChronicleData) CreationDateTimeUTC() string {
  return cd.createdDate.Format("2006-01-02 15:04:05")
}

//func LastUpdatedDateTimeUTC returns the last updated date and time (UTC) for data pertaining to the chronicle in this system
func (cd ChronicleData) LastUpdatedDateTimeUTC() string {
  return cd.lastUpdated.Format("2006-01-02 15:04:05")
}
