chronicle-be
=========

An example microservice written in golang to experiment with kube, instrumentation, CI (testing/packaging), deployment with CD, etc.  This is intended to replace the chronicle and chronicle-ms projects with something more idiomatic and more resembling production-grade code.

Background
------------

Many moons ago, there was LARP administration utility called Grapevine.  It was a Windows desktop app and it was probably written in VB6.  It tracked character sheets (XP gain/expenditures), basic information for players, allowed staff to automate sending out in-character 'rumors' before game via email, and had a basic XML-based sharing format for network games that needed to share character sheets.  

I decided to use the idea of converting an old desktop app into a series of microservices as a learning tool.  Grapevine had just enough complexity to make it an interesting and fun project.  This service is the first.  

A "chronicle" is the word for a game in White Wolf's World of Darkness parlance.

Dependencies
------------
None at the moment, more to follow as more services are added.  Some of these are being worked on in the `modeling` folder.

License
-------

BSD

Author Information
------------------

Ryan A. Morrison (ryan@ryanamorrison.com)
