package shared 

import(
  "fmt"
  "github.com/google/uuid"
  "net/mail"
  "net/url"
  "unicode/utf8"
)
//helper functions to reduce bad data errors
//func shortName checks if a name is too short and returns an error if it does
func ShortName(name string) (error) {
  //two characters may seem small but ideographic languages require characters than Roman
  if utf8.RuneCountInString(name) < 3 {
    return fmt.Errorf("chronicle name (%s) must be at least two characters or more.", name)
  }
  return nil
}
//func badEmail checks for a valid email address and returns an error if it does not find one.
func BadEmail(email string) (error) {
  if _, err := mail.ParseAddress(email); err != nil {
    return fmt.Errorf("email address (%s) did not parse correctly, check for errors and try again.", email)
  }
  return nil
}
//func badURL checks for a valid URL and returns an error if it is does not find one.
func BadURL(URL string) (error) {
  //URL is optional, allow blanks and only check if something has been submitted
  if URL != "" {
    if _, err := url.ParseRequestURI(URL); err != nil {
      return fmt.Errorf("home page URL (%s) is invalid, check for errors and try again.", URL)
    }
  }
  return nil
}
//func badUUID checks for a valid UUID and returns an error if it does not find one.
func BadUUID(uUID string) (error) {
  if _, err := uuid.Parse(uUID); err != nil {
    return fmt.Errorf("ID (%s) does not appear to be a valid UUID, check for errors and try again.", uUID)
  }
  return nil
}
