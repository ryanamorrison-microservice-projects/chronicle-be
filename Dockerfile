FROM ubuntu:latest
COPY chronicle-be /usr/bin/chronicle-be
RUN chmod a+x /usr/bin/chronicle-be
EXPOSE 8080
ENTRYPOINT ["/usr/bin/chronicle-be"]
